#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]

Based on https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7

"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json
import signal

REQUIRED_FIELDS = dict()

def quit_gracefully(*args):
    print('Quitting mock gitlab server')
    exit(0)

class S(BaseHTTPRequestHandler):
    def _set_response(self, ok=True):
        if ok:
            self.send_response(201)
        else:
            self.send_response(401)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _process_config_change(self, path):
        # We're handling a path that comes in like one of the following:
        #    /config/require/run_untagged/true
        #    /config/require/run_untagged/false
        #    /config/reset
        global REQUIRED_FIELDS

        elements = path.split("/")
        print("Elements: ", elements)
        if elements[1] != "config":
            print("Not trying to set config")
            return

        if elements[2] == "reset":
            REQUIRED_FIELDS = dict()
            return

        if elements[2] == "require" and len(elements) > 3:
            if len(elements) == 5 and elements[4] == "true":
                REQUIRED_FIELDS[elements[3]] = True
            else:
                REQUIRED_FIELDS[elements[3]] = False
        else:
            return
        print("Required fields: ")
        print(REQUIRED_FIELDS)

    def _check_required_fields(self, raw_data):
        global REQUIRED_FIELDS

        if len(REQUIRED_FIELDS.keys()) == 0:
            return True

        data = json.loads(str(raw_data))
        print(data)
        print(data['description'])
        for key in REQUIRED_FIELDS:
            try:
                setting = data[key]
            except KeyError:
                logging.info("Error - can't find %s in received data", key)
                return False
            if setting != REQUIRED_FIELDS[key]:
                logging.info("Value for received data (%s = %s) does not match expectation: %s = %s", key, setting, key, REQUIRED_FIELDS[key])
                return False
        return True

    def do_GET(self):
        global REQUIRED_FIELDS
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))

        self._process_config_change(str(self.path))

        self._set_response()
        self.wfile.write("GET request for {}\n".format(self.path).encode('utf-8'))
        self.wfile.write("Current global state {}\n".format(REQUIRED_FIELDS).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path), str(self.headers), post_data.decode('utf-8'))

        has_required_fields = self._check_required_fields(post_data.decode('utf-8'))

        payload = dict()
        if has_required_fields:
            self._set_response()  
            payload['token'] = "badf00d"
            payload['id'] = 42
            json_string = json.dumps(payload)
            self.wfile.write(bytes(json_string, 'utf-8'))
        else:
            self._set_response(False)
            payload['error'] = "missing required field(s)"
            json_string = json.dumps(payload)
            self.wfile.write(bytes(json_string, 'utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    #signal.signal(signal.SIGINT, quit_gracefully)

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
